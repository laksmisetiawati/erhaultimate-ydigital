<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Clinic;
use App\Models\Clinicgalleries;
use App\Models\Doctor;
use App\Models\Doctorschedule;
use App\Models\Pharmacist;
use App\Models\Pharmacistschedule;

class ClinicController extends Controller
{
    public function find_clinic()
    {
        $datas = [
            'url' => env('APP_URL').'find-clinic',
            'meta' => [
                'title' => 'Temukan Lokasi ERHA di Kota Kamu',
                'heading' => 'Temukan Lokasi ERHA di Kota Kamu',
                'description' => 'Temukan profil lengkap dan keahlian para dokter terbaik kami di ERHA Ultimate. Profesional berpengalaman kami, siap membantu perawatan kulit dan rambut Anda.'
            ],
            'css' => [
                'find-clinic.css',
                'tom-select.css'
            ],
            'js' => [
                'tom-select.complete.min.js'
            ],
            'cities' => City::where('deleted_at', NULL)->get()
        ];

        $param_get = isset($_GET) ? $_GET : [];

        $page_link = $datas['url'] . '?';

        $datas_list = Clinic::where('deleted_at', NULL);

        if( isset($param_get['city'] ) ) {
            $datas_list = $datas_list->where('city_id', $param_get['city']);
            $page_link =  $page_link . 'city=' . $param_get['city'] . '&';
        }

        $datas['total'] = count($datas_list->get());

        $limit = clinic_pagination_limit();
        $offset = (isset($param_get['page']) && $param_get['page'] > 1) ? ($param_get['page'] * $limit) - $limit : 0;
        $datas['list'] = $datas_list->offset($offset)->limit($limit)->get();
        
        $current_page = isset($param_get['page']) ? (int)$param_get['page'] : 1;
        $pagination_prep = clinic_pagination_prep($datas['total'], $current_page);

        $datas['pagination']['view'] = custom_pagination(
            array(
                'base' => $page_link,
                'page' => $pagination_prep['page'],
                'pages' => $pagination_prep['pages'],
                'key' => 'page',
                'next_text' => '&rsaquo;',
                'prev_text' => '&lsaquo;',
                'first_text' => '&laquo;',
                'last_text' => '&raquo;',
                'show_dots' => TRUE
            )
        );

        return view('Application.Clinic.find_clinic', $datas);
    }
    
    public function find_clinic_detail($slug)
    {
        $datas = [
            'url' => env('APP_URL').'find-clinic/',
            'meta' => [
                'title' => 'Temukan Lokasi ERHA di Kota Kamu',
                'heading' => 'Temukan Lokasi ERHA di Kota Kamu',
                'description' => 'Temukan profil lengkap dan keahlian para dokter terbaik kami di ERHA Ultimate. Profesional berpengalaman kami, siap membantu perawatan kulit dan rambut Anda.'
            ],
            'css' => [
                'find-clinic.css'
            ],
            'js' => [],
            'current' => Clinic::where('deleted_at', NULL)->where('slug', $slug)->first()
        ];

        if (!$datas['current']) {
            echo 'not found'; exit;
        }

        $datas['clinic_galleries'] = Clinicgalleries::where('clinic_id', $datas['current']['id'])->get();
        $datas['doctors'] = Doctorschedule::where('clinic_id', $datas['current']['id'])->get();
        $datas['pharmacists'] = Pharmacistschedule::where('clinic_id', $datas['current']['id'])->get();
        // pre($datas['pharmacists']);


        return view('Application.Clinic.find_clinic_detail', $datas);
    }
}
