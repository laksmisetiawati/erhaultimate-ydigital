<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctor;
use App\Models\Doctorschedule;

class DoctorController extends Controller
{
    public function find_doctor()
    {
        $datas = [
            'url' => env('APP_URL').'find-doctor',
            'meta' => [
                'title' => 'Kenali Tim Dokter Kami di ERHA',
                'heading' => 'Kenali Tim Dokter Kami di ERHA',
                'description' => 'Temukan profil lengkap dan keahlian para dokter terbaik kami di ERHA Ultimate. Profesional berpengalaman kami, siap membantu perawatan kulit dan rambut Anda.'
            ],
            'css' => [
                'find-clinic.css'
            ],
            'js' => [],
        ];

        $param_get = isset($_GET) ? $_GET : [];

        $page_link = $datas['url'] . '?';

        $datas_list = Doctor::where('deleted_at', NULL);

        if( isset($param_get['search'] ) ) {
            $datas_list = $datas_list->where('name', 'like', '%'.$param_get['search'].'%');
            $page_link =  $page_link . 'search=' . $param_get['search'] . '&';
        }

        $datas['total'] = count($datas_list->get());

        $limit = clinic_pagination_limit();
        $offset = (isset($param_get['page']) && $param_get['page'] > 1) ? ($param_get['page'] * $limit) - $limit : 0;
        $datas['list'] = $datas_list->offset($offset)->limit($limit)->get();
        
        $current_page = isset($param_get['page']) ? (int)$param_get['page'] : 1;
        $pagination_prep = clinic_pagination_prep($datas['total'], $current_page);

        $datas['pagination']['view'] = custom_pagination(
            array(
                'base' => $page_link,
                'page' => $pagination_prep['page'],
                'pages' => $pagination_prep['pages'],
                'key' => 'page',
                'next_text' => '&rsaquo;',
                'prev_text' => '&lsaquo;',
                'first_text' => '&laquo;',
                'last_text' => '&raquo;',
                'show_dots' => TRUE
            )
        );

        return view('Application.Doctor.find_doctor', $datas);
    }
    
    public function find_doctor_detail($slug)
    {
        $datas = [
            'url' => env('APP_URL').'find-doctor/',
            'meta' => [
                'title' => 'dr. Stella Adriana Limanjaya',
                'heading' => 'Kenali Tim Dokter Kami di ERHA',
                'description' => 'Temukan profil lengkap dan keahlian para dokter terbaik kami di ERHA Ultimate. Profesional berpengalaman kami, siap membantu perawatan kulit dan rambut Anda.'
            ],
            'css' => [
                'find-clinic.css',
                'tom-select.css'
            ],
            'js' => [
                'tom-select.complete.min.js'
            ],
            'current' => Doctor::where('deleted_at', NULL)->where('slug', $slug)->first()
        ];

        if (!$datas['current']) {
            echo 'not found'; exit;
        }

        $datas['schedules'] = Doctorschedule::where('doctor_id', $datas['current']['id'])->get();

        return view('Application.Doctor.find_doctor_detail', $datas);
    }
}
