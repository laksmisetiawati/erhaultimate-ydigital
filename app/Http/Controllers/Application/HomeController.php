<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function home()
    {
        $datas = [
            'url' => env('APP_URL'),
            'meta' => [
                'title' => 'ERHA Ultimate - Klinik Spesialis Kulit &amp; Rambut',
                'heading' => 'ERHA Ultimate - Klinik Spesialis Kulit &amp; Rambut',
                'description' => 'Temukan profil lengkap dan keahlian para dokter terbaik kami di ERHA Ultimate. Profesional berpengalaman kami, siap membantu perawatan kulit dan rambut Anda.'
            ],
            'css' => [],
            'js' => []
        ];

        return view('Application.Home.home', $datas);
    }

    public function truncate()
    {
        DB::table('clinic')->truncate();
        DB::table('clinic_gallery')->truncate();
        DB::table('doctor')->truncate();
        DB::table('doctor_schedule')->truncate();
        DB::table('pharmacist')->truncate();
        DB::table('pharmacist_schedule')->truncate();
    }
}
