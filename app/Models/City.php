<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    
    protected $table = "city";

    protected $primaryKey = "id";

    protected $fillable = [
        'name',
        'slug',
        'administration_code',
        'postcode',
        'area_level',
        'status',
        'created_by',
        'updated_by'
    ];

    // List of statuses
    const IS_INACTIVE = 0;
    const IS_ACTIVE = 1;
}
