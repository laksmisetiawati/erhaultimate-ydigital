<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    use HasFactory;
    
    protected $table = "clinic";

    protected $primaryKey = "id";

    protected $fillable = [
        'city_id',
        'name',
        'slug',
        'license',
        'banner',
        'address',
        'gmap_longitude',
        'gmap_latitude',
        'phone',
        'whatsapp',
        'work_time',
        'status',
        'created_by',
        'updated_by'
    ];

    public function city() {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function galleries() {
        return $this->hasMany(Clinicgalleries::class);
    }

    // List of statuses
    const IS_INACTIVE = 0;
    const IS_ACTIVE = 1;
}
