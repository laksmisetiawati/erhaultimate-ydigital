<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clinicgalleries extends Model
{
    use HasFactory;
    
    protected $table = "clinic_gallery";

    protected $primaryKey = "id";

    protected $fillable = [
        'clinic_id',
        'banner'
    ];

    public function clinic() {
        return $this->belongsTo(City::class, 'clinic_id', 'id');
    }
}
