<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    protected $table = "doctor";

    protected $primaryKey = "id";

    protected $fillable = [
        'name',
        'slug',
        'photo',
        'license',
        'experience',
        'educations',
        'clinical_interest',
        'status',
        'created_by',
        'updated_by'
    ];

    public function schedules() {
        return $this->hasMany(Doctorschedule::class);
    }

    public function clinic() {
        return $this->hasMany(Clinic::class, Doctorschedule::class, 'clinic_id', 'id');
    }

    // List of statuses
    const IS_INACTIVE = 0;
    const IS_ACTIVE = 1;
}
