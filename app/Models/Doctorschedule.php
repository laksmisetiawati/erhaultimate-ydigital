<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctorschedule extends Model
{
    use HasFactory;

    protected $table = "doctor_schedule";

    protected $primaryKey = "id";

    protected $fillable = [
        'doctor_id',
        'clinic_id',
        'work_times',
        'status',
        'created_by',
        'updated_by'
    ];

    public function doctor() {
        return $this->belongsTo(Doctor::class, 'doctor_id', 'id');
    }

    public function clinic() {
        return $this->belongsTo(Clinic::class, 'clinic_id', 'id');
    }
}
