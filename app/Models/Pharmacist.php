<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pharmacist extends Model
{
    use HasFactory;

    protected $table = "pharmacist";

    protected $primaryKey = "id";

    protected $fillable = [
        'name',
        'slug',
        'photo',
        'license',
        'educations',
        'status',
        'created_by',
        'updated_by'
    ];

    public function schedules() {
        return $this->hasMany(Pharmacistschedule::class);
    }

    // List of statuses
    const IS_INACTIVE = 0;
    const IS_ACTIVE = 1;
}
