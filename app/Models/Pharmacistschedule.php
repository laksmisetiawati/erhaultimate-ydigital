<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pharmacistschedule extends Model
{
    use HasFactory;

    protected $table = "pharmacist_schedule";

    protected $primaryKey = "id";

    protected $fillable = [
        'pharmacist_id',
        'clinic_id',
        'work_times',
        'status',
        'created_by',
        'updated_by'
    ];

    public function pharmacist() {
        return $this->belongsTo(Pharmacist::class, 'pharmacist_id', 'id');
    }

    public function clinic() {
        return $this->belongsTo(Clinic::class, 'clinic_id', 'id');
    }
}
