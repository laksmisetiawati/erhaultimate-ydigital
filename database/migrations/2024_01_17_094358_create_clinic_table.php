<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clinic', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('city_id')->index();
            $table->string('name', 100);
            $table->string('slug', 100)->unique();
            $table->string('banner', 255)->nullable();
            $table->string('work_times', 50)->nullable();
            $table->text('address')->nullable();
            $table->string('gmap_longitude', 100)->nullable();
            $table->string('gmap_latitude', 100)->nullable();
            $table->string('postcode', 50)->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('whatsapp', 50)->nullable();
            $table->json('work_time')->nullable();
            $table->integer('status')->default(1);
            $table->integer('created_by')->nullable()->index();
            $table->integer('updated_by')->nullable()->index();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clinic');
    }
};
