<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ClinicGallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contents = \File::get(base_path('public/seeders_json/clinics_gallery.json'));
        $json = json_decode(json: $contents, associative: true);

        foreach($json as $data)
        {
            $clinic = DB::table('clinic')
                ->where('name', $data['clinic_name'])
                ->first();

            DB::table('clinic_gallery')->insert([
                "clinic_id" => ($clinic) ? $clinic->id : 0,
                "banner" => $data['banner']
            ]);
        }
    }
}