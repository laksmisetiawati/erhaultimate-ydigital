<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ClinicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * NOTES :
     * Please make sure the global helper loaded
     *
     * @return void
     */
    public function run()
    {
        $contents = \File::get(base_path('public/seeders_json/clinics.json'));
        $json = json_decode(json: $contents, associative: true);
        // pre($json);

        foreach($json as $data)
        {            
            $city = DB::table('city')
                ->where('name', $data['city'])
                ->where('area_level', $data['city_area_level'])
                ->first();

            $slug = create_slug('clinic', $data['name']);

            DB::table('clinic')->insert([
                "city_id" => ($city) ? $city->id : 0,
                "name" => $data['name'],
                "slug" => $slug,
                "license" => $data['license'],
                "banner" => $data['banner'],
                "work_time" => json_encode($data['work_times']),
                "address" => $data['address'],
                "gmap_longitude" => $data['gmap_longitude'],
                "gmap_latitude" => $data['gmap_latitude'],
                "phone" => $data['phone'],
                "whatsapp" => $data['whatsapp'],
                "created_by" => 1,
                "updated_by" => 1
            ]);
        }
    }
}