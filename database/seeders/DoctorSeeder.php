<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * NOTES :
     * Please make sure the global helper loaded
     *
     * @return void
     */
    public function run(): void
    {
        $contents = \File::get(base_path('public/seeders_json/doctors.json'));
        $json = json_decode(json: $contents, associative: true);

        foreach($json as $data)
        {
            $slug = create_slug('doctor', $data['name']);

            DB::table('doctor')->insert([
                "name" => $data['name'],
                "slug" => $slug,
                "photo" => $data['photo'],
                "license" => $data['license'],
                "experience" => $data['experience'],
                "educations" => json_encode($data['educations']),
                "clinical_interest" => json_encode($data['clinical_interest']),
                "created_by" => 1,
                "updated_by" => 1
            ]);
        }
    }
}
