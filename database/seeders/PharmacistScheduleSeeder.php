<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PharmacistScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $contents = \File::get(base_path('public/seeders_json/pharmacist_schedule.json'));
        $json = json_decode(json: $contents, associative: true);

        foreach($json as $data)
        {
            $clinic = DB::table('clinic')
                ->where('name', $data['clinic_name'])
                ->first();

            $pharmacist = DB::table('pharmacist')
                ->where('name', $data['pharmacist_name'])
                ->first();

            DB::table('pharmacist_schedule')->insert([
                "pharmacist_id" => ($pharmacist) ? $pharmacist->id : 0,
                "clinic_id" => ($clinic) ? $clinic->id : 0,
                "work_times" => json_encode($data['work_times']),
                "created_by" => 1,
                "updated_by" => 1
            ]);
        }
    }
}
