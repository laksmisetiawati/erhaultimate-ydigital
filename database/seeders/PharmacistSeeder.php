<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PharmacistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * NOTES :
     * Please make sure the global helper loaded
     *
     * @return void
     */
    public function run(): void
    {
        $contents = \File::get(base_path('public/seeders_json/pharmacist.json'));
        $json = json_decode(json: $contents, associative: true);

        foreach($json as $data)
        {
            $slug = create_slug('pharmacist', $data['name']);

            DB::table('pharmacist')->insert([
                "name" => $data['name'],
                "slug" => $slug,
                "photo" => $data['photo'],
                "license" => $data['license'],
                "created_by" => 1,
                "updated_by" => 1
            ]);
        }
    }
}
