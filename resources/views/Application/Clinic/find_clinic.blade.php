@extends('Application.Layout.app')

@section('content')

<section class="section find-clinic-section">

    <div class="find-clinic-banner">
        <div class="row">
            <div class="head-banner banner-find-clinic" style="">
                <div class="container find-clinic-head">
                    
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ env('APP_URL') }}" title="Beranda">
                                    Beranda
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                <span>
                                    Temukan Klinik
                                </span>
                            </li>
                        </ol>
                    </nav>

                    <div class="title-head">
                        <h1 class="section-group__title">Temukan Klinik</h1>
                    </div>

                    <div class="col-lg-4 col-md-6 left">
                        <div class="form-group">
                            <div class="ts-form-wrapper with-dropdown">
                                <select id="select-city" name="city" tabindex="-1" class="tomselecteds ts-hidden-accessibles">
                                    <option value="">Semua Kota</option>
                                    <?php foreach($cities as $city) { ?>
                                        <option value="{{ $city['id'] }}" {{ isset($_GET['city']) && $_GET['city'] == $city['id'] ? 'selected' : '' }}>{{ $city['name'] }}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-6 right">
                        <ul>
                            <li><a href="{{ env('APP_URL').'find-clinic/' }}" class="btn btn-border-white active">Lokasi</a></li>
                            <li><a href="{{ env('APP_URL').'find-doctor/' }}" class="btn btn-border-white">Dokter</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container content-area">
        <?php if(count($list) > 0) { ?>

            <div class="row">
                <?php foreach($list as $dt) { ?>
                    <div class="col-sm-6 card-clinic">
                        <div class="card">
                            <div class="card-body">
                                <div class="image-wrapper">
                                    <img src="{{ $dt['banner'] ? $dt['banner'] : asset('/img/layout/profil-clinic-3.png') }}" data-src="{{ $dt['banner'] ? $dt['banner'] : asset('/img/layout/profil-clinic-3.png') }}" class="img-avatar card-img" class="card-img" alt="doctor">
                                </div>
                                <div class="content-wrapper">
                                    <h5 class="card-title">{{ $dt['name'] }}</h5>
                                    <p class="card-text">{{ $dt['address'] }}</p>
                                    <a href="{{ env('APP_URL').'find-clinic/' }}{{ $dt['slug'] }}">Lihat Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="page-pagination center-flex">
                    <?php if($total != 0) { ?>
                        <div class="pagination new">
                            <?php echo $pagination['view']; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

        <?php } else { ?>

            <div class="notes">
                <div class="notes-icon">
                    <i class="bi bi-info-circle"></i>ⓘ
                </div>
                <div class="notes-text">
                    <p>Klinik kami belum hadir di kota yang kamu pilih. <br>Tunggu kami ya!</p>
                </div>
            </div>

        <?php } ?>
    </div>
    
</section>

<script>
    $("#select-city").change(function(){
        var val = $(this).val()
        if(val!="") {
            selected_city = val;
            window.location.replace("/find-clinic?city="+selected_city);
        } else {
            window.location.replace("/find-clinic");
        }
    })
    $(function() {
        var select = new TomSelect("#select-city", {
            maxOptions: 9999, 
            allowEmptyOption: true,
            sortField: {
                field: "text",
                direction: "asc"
            },
        });
    });
</script>

@endsection