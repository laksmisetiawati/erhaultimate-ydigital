@extends('Application.Layout.app')

@section('content')

<section class="section detail-find-clinic-doctor-section m-0 clinic-detail">

    <div class="container container-m-fluid">
        <div class="row">
            <div class="btn-back-wrapper">
                <a href="#" class="btn-back img-white">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-arrow-left-short btn-back-icon btn-primary" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"></path>
                    </svg>
                </a>
            </div>
            <div class="head">
                <div class="img-position image-wrapper">
                    <picture>
                        <source media="(max-width: 481px)" srcset="{{ $current['banner'] ? $current['banner'] : asset('/img/layout/profil-clinic-3.png') }}">
                        <img src="{{ $current['banner'] ? $current['banner'] : asset('/img/layout/profil-clinic-3.png') }}" class="img-avatar card-img" alt="image">
                    </picture>
                </div>
                <div class="description-detail">
                    <h4 class="title">{{ $current['name'] }}</h4>
                    <p class="legality">Surat Izin Klinik {{ $current['license'] }}</p>
                    <p class="address">{{ $current['address'] }}</p>
                    <?php if($current['gmap_latitude'] && $current['gmap_longitude']) { ?>
                        <a href="http://maps.google.com/maps?z=12&amp;t=m&amp;q=loc:{{ $current['gmap_latitude'] }}+{{ $current['gmap_longitude'] }}" target="_blank" class="btn-detail">Lihat Peta</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row desktop-show">

            <div class="col-md-12 body row">

                <div class="col-md-6">
                    <div class="content">
                        <h4 class="title">Informasi</h4>
                        <?php if($current['whatsapp']) { ?>
                            <div class="contact">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                    <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"></path>
                                </svg>
                                <a class="phone" target="_blank" href="https://wa.me/{{ str_replace('+', '', $current['whatsapp']) }}">{{ $current['whatsapp'] }}</a>
                            </div>
                        <?php } ?>
                        <?php if($current['phone']) { ?>
                            <div class="contact">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-telephone" viewBox="0 0 16 16">
                                    <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"></path>
                                </svg>
                                <a class="phone" target="_blank" href="tel:{{ $current['phone'] }}">{{ $current['phone'] }}</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <?php if($pharmacists) { ?>
                    <div class="col-md-6">
                        <div class="content">
                            <h4 class="title">Nama Apoteker</h4>
                            <?php foreach($pharmacists as $phar) { ?>
                                <div class="worker">
                                    <p class="name">{{ $phar['pharmacist']['name'] }}</p>
                                    <p class="legality">{{ $phar['pharmacist']['license'] }}</p>
                                    <p class="schedule">
                                        <?php $phar_work_times = json_decode($phar['work_times']); ?>
                                        <?php foreach($phar_work_times as $wt) { ?>
                                            {{ $wt }}
                                        <?php } ?>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="col-md-6">
                    <div class="content">
                        <h4 class="title">Waktu Kerja</h4>
                        <div class="list">
                            <?php $work_times = json_decode($current['work_time']); ?>
                            <?php foreach($work_times as $work_time) { ?>
                                <p class="job-list">{{ $work_time }}</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <?php if($clinic_galleries) { ?>
                    <div class="col-md-6">
                        <div class="content">
                            <h4 class="title">Galeri Klinik</h4>
                            <div class="galeri-clinic">
                                <ul>
                                    <?php foreach($clinic_galleries as $gallery) { ?>
                                        <li>
                                            <a class="showModal" data-image="{{ $gallery['banner'] }}" data-bs-toggle="modal" data-bs-target="#image-popup-desktop">
                                                <img data-src="{{ $gallery['banner'] }}" alt="galery">
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <?php if($doctors) { ?>
                <div class="col-md-12 footer mt-4">
                    <h4 class="title">Dokter</h4>
                    <div class="list-doctor-wrapper">

                        <?php foreach($doctors as $doc) { ?>
                            <div class="card-doctor">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="itm-card-left image-wrapper">
                                            <img src="{{ $doc['doctor']['photo'] ? $doc['doctor']['photo'] : asset('/img/layout/profil-doctor.jpeg') }}" data-src="{{ $doc['doctor']['photo'] ? $doc['doctor']['photo'] : asset('/img/layout/profil-doctor.jpeg') }}" class="promotion-logo" alt="doctor">
                                        </div>
                                        <div class="car-con itm-card-right">
                                            <div class="card-body card-cstm">
                                                <div class="info-wrapper">
                                                    <h5 class="card-title">
                                                        {{ $doc['doctor']['name'] }}
                                                    </h5>
                                                    <p class="interest">
                                                        <?php
                                                            foreach(json_decode($doc['doctor']['clinical_interest']) as $k => $ci) {
                                                                echo ($k > 0) ? ', ' . $ci : $ci;
                                                            }
                                                        ?>
                                                    </p>
                                                </div>
                                                <a href="{{ env('APP_URL').'find-doctor/'.$doc['doctor']['slug'] }}" class="btn-card">Lihat Detail</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="row mobile-show">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Informasi</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false" tabindex="-1">Dokter</button>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="body">

                        <div class="col-md-6">
                            <div class="content">
                                <?php if($current['whatsapp']) { ?>
                                    <div class="contact">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                            <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"></path>
                                        </svg>
                                        <a class="phone" target="_blank" href="https://wa.me/{{ str_replace('+', '', $current['whatsapp']) }}">{{ $current['whatsapp'] }}</a>
                                    </div>
                                <?php } ?>
                                <?php if($current['phone']) { ?>
                                    <div class="contact">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-telephone" viewBox="0 0 16 16">
                                            <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"></path>
                                        </svg>
                                        <a class="phone" target="_blank" href="tel:{{ $current['phone'] }}">{{ $current['phone'] }}</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="content">
                                <h4 class="title">Nama Apoteker</h4>
                                <?php if($pharmacists) { ?>
                                    <?php foreach($pharmacists as $phar) { ?>
                                        <div class="worker">
                                            <p class="name">{{ $phar['pharmacist']['name'] }}</p>
                                            <p class="legality">{{ $phar['pharmacist']['license'] }}</p>
                                            <p class="schedule">
                                                <?php $phar_work_times = json_decode($phar['work_times']); ?>
                                                <?php foreach($phar_work_times as $wt) { ?>
                                                    {{ $wt }}
                                                <?php } ?>
                                            </p>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="content">
                                <h4 class="title">Waktu Kerja</h4>
                                <div class="list">
                                    <?php $work_times = json_decode($current['work_time']); ?>
                                    <?php foreach($work_times as $work_time) { ?>
                                        <p class="job-list">{{ $work_time }}</p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <?php if($clinic_galleries) { ?>
                                <div class="content">
                                    <h4 class="title">Galeri Klinik</h4>
                                    <div class="galeri-clinic">
                                        <ul>
                                            <?php foreach($clinic_galleries as $gallery) { ?>
                                                <li>
                                                    <a class="showModal" data-image="{{ $gallery['banner'] }}" data-bs-toggle="modal" data-bs-target="#image-popup-mobile">
                                                        <img data-src="{{ $gallery['banner'] }}" alt="galery">
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                    </div>
                </div>

                <?php if($doctors) { ?>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="footer">
                            <div class="list-doctor-wrapper">
                                <?php foreach($doctors as $doc) { ?>
                                    <div class="card-doctor">
                                        <div class="card">
                                            <div class="row no-gutters">
                                                <div class="itm-card-left image-wrapper">
                                                    <img src="{{ $doc['doctor']['photo'] ? $doc['doctor']['photo'] : asset('/img/layout/profil-doctor.jpeg') }}" data-src="{{ $doc['doctor']['photo'] ? $doc['doctor']['photo'] : asset('/img/layout/profil-doctor.jpeg') }}" class="promotion-logo" alt="doctor">
                                                </div>
                                                <div class="car-con itm-card-right">
                                                    <div class="card-body card-cstm">
                                                        <div class="info-wrapper">
                                                            <h5 class="card-title">
                                                                {{ $doc['doctor']['name'] }}
                                                            </h5>
                                                            <p class="interest">
                                                                <?php
                                                                    foreach(json_decode($doc['doctor']['clinical_interest']) as $k => $ci) {
                                                                        echo ($k > 0) ? ', ' . $ci : $ci;
                                                                    }
                                                                ?>
                                                            </p>
                                                        </div>
                                                        <a href="{{ env('APP_URL').'find-doctor/'.$doc['doctor']['slug'] }}" class="btn-card">Lihat Detail</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>                            
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</section>

<script>
    $(".showModal").click(function(){
        var content = $(this).data('image')
        content = `<img src="${content}" class="find-clinic-gallery-full" alt="galery"/>`
        $("#image-popup-desktop .modal-body").html(content)
        $("#image-popup-mobile .modal-body").html(content)
    })
</script>

<div class="modal fade bd-example-modal-lg" id="image-popup-desktop" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <img src="{{ asset('/img/doctor/20230216063921-0.jpg') }}" class="find-clinic-gallery-full" alt="galery">
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="image-popup-mobile" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"><img src="{{ asset('/img/doctor/20230216063921-0.jpg') }}" class="find-clinic-gallery-full" alt="galery"></div>
        </div>
    </div>
</div>

@endsection