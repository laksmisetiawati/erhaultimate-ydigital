@extends('Application.Layout.app')

@section('content')

<section class="section find-clinic-section">

    <div class="find-clinic-banner">
        <div class="row">

            <div class="head-banner banner-find-clinic" style="">
                <div class="container find-clinic-head">
                    
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ env('APP_URL') }}" title="Beranda">
                                    Beranda
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                <span>
                                    Temukan Dokter
                                </span>
                            </li>
                        </ol>
                    </nav>

                    <div class="title-head">
                        <h1 class="section-group__title">Ahli Dermatologi Kami</h1>
                    </div>

                    <div class="col-lg-4 col-md-6 left">
                        <div class="form-group">
                            <form method="get" class="ts-form-wrapper" id="form-search">
                                <svg class="form-icon" xmlns="http://www.w3.org/2000/svg" height="2em" viewBox="0 0 24 24" fill="#fff">
                                    <path d="M16.6725 16.6412L21 21M19 11C19 15.4183 15.4183 19 11 19C6.58172 19 3 15.4183 3 11C3 6.58172 6.58172 3 11 3C15.4183 3 19 6.58172 19 11Z" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                </svg>
                                <div class="ts-wrapper">
                                    <div class="ts-control">
                                        <input type="text" name="search" value="" class="search-bar" placeholder="Cari berdasarkan nama">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-6 right">
                        <ul>
                            <li><a href="{{ env('APP_URL').'find-clinic/' }}" class="btn btn-border-white">Lokasi</a></li>
                            <li><a href="{{ env('APP_URL').'find-doctor/' }}" class="btn btn-border-white active">Dokter</a></li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="container content-area">
        <?php if(count($list) > 0) { ?>
            <div class="row">
                <?php foreach($list as $dt) { ?>
                    <div class="col-sm-6 card-clinic">
                        <div class="card">
                            <div class="card-body">
                                <div class="image-wrapper">
                                    <img src="{{ $dt['photo'] ? $dt['photo'] : asset('/img/layout/profil-doctor.jpeg') }}" data-src="{{ $dt['photo'] ? $dt['photo'] : asset('/img/layout/profil-doctor.jpeg') }}" class="img-avatar card-img" class="card-img" alt="doctor">
                                </div>
                                <div class="content-wrapper">
                                    <h5 class="card-title">{{ $dt['name'] }}</h5>
                                    <p class="card-text">
                                        <?php
                                            foreach(json_decode($dt['clinical_interest']) as $k => $ci) {
                                                echo ($k > 0) ? ', ' . $ci : $ci;
                                            }
                                        ?>
                                    </p>
                                    <a href="{{ env('APP_URL').'find-doctor/' }}{{ $dt['slug'] }}">Lihat Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="page-pagination center-flex">
                <?php if($total != 0) { ?>
                    <div class="pagination new">
                        <?php echo $pagination['view']; ?>
                    </div>
                <?php } ?>
            </div>

            <?php /*template*/ /*<div class="page-pagination center-flex">
                <ul class="pagination">
    
                    <li class="page-item disabled arrow-custom" aria-disabled="true" aria-label="« Previous">
                        <span class="page-link">
                            <svg class="back-arow" viewBox="-4.5 0 20 20" style="width:30px" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Dribbble-Light-Preview" transform="translate(-385.000000, -6679.000000)" fill="#000000">
                                        <g id="icons" transform="translate(56.000000, 160.000000)">
                                            <path fill="#f58220" d="M338.61,6539 L340,6537.594 L331.739,6528.987 L332.62,6528.069 L332.615,6528.074 L339.955,6520.427 L338.586,6519 C336.557,6521.113 330.893,6527.014 329,6528.987 C330.406,6530.453 329.035,6529.024 338.61,6539" id="arrow_left-[#334]"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </span>
                    </li>

                    <li data-key="1" class="page-item active"><a class="page-link" href="{{ env('APP_URL').'find-doctor/' }}?page=1">1</a></li>
                    <li data-key="2" class="page-item "><a class="page-link" href="{{ env('APP_URL').'find-doctor/' }}?page=2">2</a></li>
                    <li data-key="3" class="page-item "><a class="page-link" href="{{ env('APP_URL').'find-doctor/' }}?page=3">3</a></li>
                    <li data-key="4" class="page-item "><a class="page-link" href="{{ env('APP_URL').'find-doctor/' }}?page=4">4</a></li>
    
                    <li class="page-item disabled"><span class="page-link">...</span></li>

                    <li class="page-item"><a class="page-link" href="{{ env('APP_URL').'find-doctor/' }}?page=65">65</a></li>

                    <li class="page-item ">
                        <a class="" href="{{ env('APP_URL').'find-doctor/' }}?page=2" rel="next" aria-label="Next »">
                            <svg class="back-arow" style="width:30px!important" viewBox="-4.5 0 20 20" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Dribbble-Light-Preview" transform="translate(-425.000000, -6679.000000)" fill="#000000">
                                        <g id="icons" transform="translate(56.000000, 160.000000)">
                                            <path fill="#f58220" d="M370.39,6519 L369,6520.406 L377.261,6529.013 L376.38,6529.931 L376.385,6529.926 L369.045,6537.573 L370.414,6539 C372.443,6536.887 378.107,6530.986 380,6529.013 C378.594,6527.547 379.965,6528.976 370.39,6519" id="arrow_right-[#333]"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                    
                </ul>
            </div>*/ ?>

        <?php } else { ?>

            <div class="notes">
                <div class="notes-icon">
                    <i class="bi bi-info-circle"></i>ⓘ
                </div>
                <div class="notes-text">
                    <p>Dokter kami sedang proses input. <br>Tunggu kami ya!</p>
                </div>
            </div>

        <?php } ?>

    </div>

</section>

@endsection