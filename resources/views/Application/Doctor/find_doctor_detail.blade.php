@extends('Application.Layout.app')

@section('content')

<section class="section detail-find-clinic-doctor-section">

    <div class="container">
        <div class="row">
            <div class="btn-back-wrapper">
                <a href="#" class="btn-back">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-arrow-left-short btn-back-icon btn-primary" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"></path>
                    </svg>
                </a>
            </div>
        </div>
    </div>

    <div class="container container-m-fluid">
        <div class="row">
            <div class="head">
                <div class="img-position image-wrapper">
                    <picture>
                        <img src="{{ $current['photo'] ? $current['photo'] : asset('/img/layout/profil-doctor.jpeg') }}" class="img-avatar card-img" alt="image">
                    </picture>
                </div>
                <div class="description-detail">
                    <h4 class="title">{{ $current['name'] }}</h4>
                    <?php echo $current['license'] ? '<p class="experience">No STR ' . $current['license'] . '</p>' : ''; ?>
                    <p class="experience">{{ $current['experience'] > 0 ? $current['experience'] . ' Years' : '0 Year' }} Experience</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container mb-5 mb-lg-0">
        <div class="row">
            <div class="col-md-12 body">
                <div class="row">

                    <div class="col-md-6 left p-0">
                        <?php if($current['clinical_interest']) { ?>
                            <div class="content">
                                <h4 class="title">Clinical Interest</h4>
                                <div class="description">
                                    <p>
                                        <?php
                                            foreach(json_decode($current['clinical_interest']) as $k => $ci) {
                                                echo ($k > 0) ? ', ' . $ci : $ci;
                                            }
                                        ?>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                        
                        <?php if($current['educations']) { ?>
                            <div class="content">
                                <h4 class="title">Pendidikan</h4>
                                <div class="list-study">
                                    <?php
                                        foreach(json_decode($current['educations']) as $k => $edu) {
                                            echo '<p class="name-university">' . $edu . '</p>';
                                        }
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                
                    <div class="col-md-6 right">
                        <div class="content">
                            <h4 class="title">Lokasi Klinik</h4>

                            <div class="ts-form-wrapper with-dropdown">
                                <select id="select-schedule" name="schedule" tabindex="-1" class="tomselecteds ts-hidden-accessibles">
                                    <?php foreach($schedules as $k => $schedule) { ?>
                                        <option value="{{ $schedule['clinic']['id'] }}">
                                            {{ $schedule['clinic']['name'] }}
                                        </option>
                                    <?php } ?>
                                </select>
                                
                                <!-- <div class="ts-wrapper single full has-items input-hidden">
                                    <div class="ts-control">
                                        <div data-value="25" class="item" data-ts-item="">
                                            Bandung - Soekarno Hatta
                                        </div>
                                        <input type="select-one" autocomplete="off" size="1" tabindex="0" role="combobox" aria-haspopup="listbox" aria-expanded="false" aria-controls="select-schedule-ts-dropdown" id="select-schedule-ts-control" aria-activedescendant="select-schedule-opt-1">
                                    </div>

                                    <div class="ts-dropdown single" style="display: none; visibility: visible;">
                                        <div role="listbox" tabindex="-1" class="ts-dropdown-content" id="select-schedule-ts-dropdown">
                                            <div data-selectable="" data-value="25" class="option selected active" role="option" id="select-schedule-opt-1" aria-selected="true">
                                                Bandung - Soekarno Hatta
                                            </div>
                                            <div data-selectable="" data-value="123" class="option" role="option" id="select-schedule-opt-2">
                                                Cimahi
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <svg fill="#54565A" class="form-icon dropdown-icon" xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 330 330" xml:space="preserve">
                                    <path id="XMLID_225_" d="M325.607,79.393c-5.857-5.857-15.355-5.858-21.213,0.001l-139.39,139.393L25.607,79.393
                                    c-5.857-5.857-15.355-5.858-21.213,0.001c-5.858,5.858-5.858,15.355,0,21.213l150.004,150c2.813,2.813,6.628,4.393,10.606,4.393
                                    s7.794-1.581,10.606-4.394l149.996-150C331.465,94.749,331.465,85.251,325.607,79.393z"></path>
                                </svg> -->
                            </div>

                            <?php if($current['schedules']) { ?>
                                <div class="tbl-list-schedule">
                                    <?php foreach($current['schedules'] as $k => $schedule) { ?>
                                        <table class="table clinic-time {{ $k > 0 ? 'hidden' : '' }} " id="{{ $schedule ['clinic_id'] }}" aria-describedby="schedule table">
                                            <colgroup>
                                                <col span="1">
                                                <col span="1">
                                            </colgroup>

                                            <thead style="display:none">
                                                <tr>
                                                    <th scope="col">Day</th>
                                                    <th scope="col">Hour</th>
                                                </tr>
                                            </thead>

                                            <?php $work_time = json_decode($schedule ['work_times']); ?>
                                            <tbody>
                                                <tr>
                                                    <td class="day">senin</td>
                                                    <td class="hour">{{ $work_time[0]->MONDAY }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">selasa</td>
                                                    <td class="hour">{{ $work_time[0]->TUESDAY }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">rabu</td>
                                                    <td class="hour">{{ $work_time[0]->WEDNESDAY }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">kamis</td>
                                                    <td class="hour">{{ $work_time[0]->THURSDAY }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">jumat</td>
                                                    <td class="hour">{{ $work_time[0]->FRIDAY }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">sabtu</td>
                                                    <td class="hour">{{ $work_time[0]->SATURDAY }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">minggu</td>
                                                    <td class="hour">{{ $work_time[0]->SUNDAY }}</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <?php /*<table class="table clinic-time  " id="25" aria-describedby="schedule table">
                                            <colgroup>
                                                <col span="1">
                                                <col span="1">
                                            </colgroup>

                                            <thead style="display:none">
                                                <tr>
                                                    <th scope="col">Day</th>
                                                    <th scope="col">Hour</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td class="day">senin</td>
                                                    <td class="hour">14:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">selasa</td>
                                                    <td class="hour"> 09:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">rabu</td>
                                                    <td class="hour">09:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">kamis</td>
                                                    <td class="hour">09:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">jumat</td>
                                                    <td class="hour"> 09:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">sabtu</td>
                                                    <td class="hour">09:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">minggu</td>
                                                    <td class="hour">14:00 - 19:00</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table class="table clinic-time  hidden  " id="123" aria-describedby="schedule table">
                                            <colgroup>
                                                <col span="1">
                                                <col span="1">
                                            </colgroup>
                                            <thead style="display:none">
                                                <tr>
                                                    <th scope="col">Day</th>
                                                    <th scope="col">Hour</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="day">senin</td>
                                                    <td class="hour">-</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">selasa</td>
                                                    <td class="hour">-</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">rabu</td>
                                                    <td class="hour">09:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">kamis</td>
                                                    <td class="hour">09:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">jumat</td>
                                                    <td class="hour">09:00 - 19:00</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">sabtu</td>
                                                    <td class="hour">-</td>
                                                </tr>
                                                <tr>
                                                    <td class="day">minggu</td>
                                                    <td class="hour">-</td>
                                                </tr>
                                            </tbody>
                                        </table>*/ ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-6"></div>
            </div>
        </div>
    </div>

</section>

<script>
    $("#select-schedule").change(function(){
        var id = $(this).val()
        $(".clinic-time").each(function () {
            if($(this).attr('id')==id){
                $(this).removeClass("hidden");
            } else {
                $(this).addClass("hidden");
            }
        })
    })
    $(function() {
        var select = new TomSelect("#select-schedule", {
            create: false,
            allowEmptyOption: true,
            // controlInput: null,
            sortField: {
                field: "text",
                direction: "asc"
            },
        });
    });
</script>

@endsection