<!DOCTYPE html>

<html lang="en">
    <head lang="id">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=1" name="viewport">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        
        <title>{{ $meta["title"] }}</title>
        
        <meta name="description" content="{{ $meta["description"] }}">
        <link rel="canonical" href="{{ $url }}">
        <meta name="robots" content="index,follow">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:image" content="{{ asset('/img/layout/logo.png') }}">
        <meta name="twitter:site" content="@erhabrightening">
        <meta name="twitter:creator" content="@erhabrightening">
        <meta name="viewport" content="initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=yes, width=device-width">
        <meta property="og:site_name" content="ERHA">
        <meta property="og:title" content="{{ $meta["title"] }}">
        <meta property="og:description" content="{{ $meta["description"] }}">
        <meta property="og:image" content="{{ asset('/img/layout/logo.png') }}">
        <meta property="og:url" content="{{ $url }}">
        <meta property="og:type" content="Website">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="630">
        <meta name="twitter:title" content="{{ $meta["title"] }}">
        <meta name="twitter:description" content="{{ $meta["description"] }}">

        <script src="{{ asset('/js/jquery-3.6.3.min.js') }}"></script>

        <link rel="shortcut icon" href="{{ asset('/img/layout/favicon.png') }}">

        <link rel="preconnect" href="https://d3sgbq9gctgf5o.cloudfront.net/">
        <link rel="dns-prefetch" href="https://d3sgbq9gctgf5o.cloudfront.net/">
        <link rel="preconnect" href="https://cdnjs.cloudflare.com/">
        <link rel="dns-prefetch" href="https://cdnjs.cloudflare.com/">

        <link href="{{ asset('/css/fonts.css') }}" rel="stylesheet" as="style" onload="this.rel=&#39;stylesheet&#39;">
        <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" as="style" onload="this.rel=&#39;stylesheet&#39;">
        <link rel="preload" as="image" href="{{ asset('/img/layout/logo.png') }}">
        <link href="{{ asset('/css/critical.css') }}" rel="stylesheet" as="style" onload="this.rel=&#39;stylesheet&#39;">
        <link href="{{ asset('/css/common.css') }}" rel="stylesheet" as="style" onload="this.rel=&#39;stylesheet&#39;">
        <link href="{{ asset('/css/slick.min.css') }}" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" rel="stylesheet" as="style" onload="this.rel=&#39;stylesheet&#39;">
        <link href="{{ asset('/css/slick-theme.min.css') }}" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" rel="stylesheet" as="style" onload="this.rel=&#39;stylesheet&#39;">

        @foreach ($css as $c)
            <link rel="stylesheet" type="text/css" href="{{ asset('/css/'.$c) }}" />
        @endforeach

        <style>
            section {content-visibility:auto}
        </style>

        <!--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
        <!--WARNING: Respond.js doesn't work if you view the page via file://-->
        <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->

        <script>
            window.sentry_dsn = "";
            window.app_env = "{{ env('APP_ENV') }}";
        </script>

        <script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "WebSite",
                "name": "ERHA",
                "url": "https://erhaultimate.co.id"
            }
        </script>

        <script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"name":"Beranda","item":"https:\/\/erhaultimate.co.id"},{"@type":"ListItem","position":2,"name":"Temukan Dokter","item":"{{ $url }}"}]}</script>

        <script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"Organization","name":"ERHA Ultimate - Klinik Spesialis Kulit &amp; Rambut","url":"{{ $url }}","logo":{"@type":"ImageObject","url":"{{ asset('/img/layout/logo.png') }}"}}</script><link rel="alternate" href="{{ $url }}" hreflang="x-default">

        <link rel="alternate" href="{{ $url }}" hreflang="en">
        <link rel="preconnect" href="https://www.google.com/">
        <link rel="preconnect" href="https://www.gstatic.com/" crossorigin="">

        <script>
            window.siteUrl = "https://erhaultimate.co.id";
        </script>

        <!-- Google Tag Manager -->
        <!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=HtJe5wDmvjDzS5jte33_7A&gtm_preview=env-1&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer', 'GTM-5P5FXQMG');</script> -->
        <!-- End Google Tag Manager -->

    </head>

    <!--[if IE 7]><body class="ie7 lt-ie8 lt-ie9 lt-ie10"><![endif]-->
    <!--[if IE 8]><body class="ie8 lt-ie9 lt-ie10"><![endif]-->
    <!--[if IE 9]><body class="ie9 lt-ie10"><![endif]-->

    <body>

    <!-- Google Tag Manager (noscript) -->
    <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5P5FXQMG&gtm_auth=HtJe5wDmvjDzS5jte33_7A&gtm_preview=env-1&gtm_cookies_win=x"
                height=""0"" width=""0"" style=""display:none;visibility:hidden""></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <header class="header" id="header">
        <nav class="container">
            <div class="header-content position-relative">
                <div class="logo">
                    <a href="{{ env('APP_URL') }}">
                        <img id="img-logo" src="{{ asset('/img/layout/logo.png') }}" alt="ERHA Ultimate - Klinik Spesialis Kulit &amp;amp; Rambut">
                    </a>
                </div>

                <ul class="menu sub-menu--slideLeft mx-lg-3">
                    <li class="menu-item  menu-item-has-children   ">
                        <div class="dropdown">
                            <a href="javascript:void(0)">
                                Solusi Sesuai Problem 
                                <span>
                                    <img class="icon-arrow" data-src="{{ asset('/img/layout/arrow-5.png') }}" src="{{ asset('/img/layout/arrow-5.png') }}" alt="arrow">
                                </span>
                            </a>
                            
                            <div>
                                <div class="dropdown-content">
                                    <a href="https://erhaultimate.co.id/acne-center" target="_self" class="title-subtitle ">
                                        Acne Center <span>Kulit Jerawat &amp; Bekas Jerawat</span> 
                                    </a>
                                    <a href="https://erhaultimate.co.id/anti-aging-center" target="_self" class="title-subtitle ">
                                        Anti Aging Center <span>Kulit Kendur &amp; Garis Halus</span> 
                                    </a>
                                    <a href="https://erhaultimate.co.id/brightening-center" target="_self" class="title-subtitle ">
                                        Brightening Center <span>Kulit Kusam &amp; Noda Hitam</span> 
                                    </a>
                                    <a href="https://erhaultimate.co.id/hair-care-center" target="_self" class="title-subtitle ">
                                        Hair Care Center <span>Rambut Rontok &amp; Botak</span> 
                                    </a>
                                    <a href="https://erhaultimate.co.id/make-over-center" target="_self" class="title-subtitle ">
                                        Make Over Center <span>Bentuk Wajah &amp; Badan Kurang Ideal</span> 
                                    </a>
                                    <a href="https://erhaultimate.co.id/atopy-and-skin-disease-center" target="_self" class="title-subtitle ">
                                        Atopy &amp; Skin Disease Center <span>Dermatitis Atopik &amp; Penyakit Kulit</span> 
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="menu-item   ">
                        <a href="https://erhaultimate.co.id/promo" target="_self">
                            Promo Spesial
                        </a>
                    </li>
                    
                    <li class="menu-item   ">
                        <a href="https://erhaultimate.co.id/erha-story" target="_self">
                            Kisah ERHA
                        </a>
                    </li>
                    
                    <li class="menu-item   ">
                        <a href="https://erhaultimate.co.id/article" target="_self">
                            Info Terkini
                        </a>
                    </li>
                </ul>

                <ul class="menu-icons ms-md-4 ms-5">
                    <li class="header-input">
                        <div class="search-input desktop ">
                            <span class="close-search d-none" data-target="desktop">
                                <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 384 512" fill="currentColor"><path d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"></path></svg>
                            </span>
                            <form action="https://erhaultimate.co.id/search" method="get" id="form-search-desktop" class="form-search">
                                <input type="text" name="search" value="" class="header-search-bar input-search" placeholder="Pencarian">
                            </form>
                        </div>
                    </li>

                    <li class="header-icons">
                        <div class="search " data-target="desktop"> 
                            <a href="#" class="btn-search" data-target="desktop">
                                <img src="{{ asset('/img/layout/icon-search.png') }}" alt="Erha Search">
                            </a>
                        </div>
                    </li>
                    <li class="header-icons">
                        <div class="profile">
                            <a href="https://erhaultimate.co.id/profile"> 
                                <img src="{{ asset('/img/layout/icon-user.png') }}" alt="User Profile">
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <header class="header-mobile">
        <nav class="container navigation position-relative">
            <div class="left-items-wrapper d-flex align-items-center">
                <button id="open-nav" class="mobile-menu-btn me-1"><img src="{{ asset('/img/layout/mobile-menu.png') }}" alt="menu"></button>
                <div class="logo ms-4">
                    <a href="{{ env('APP_URL') }}">
                        <img src="{{ asset('/img/layout/logo.png') }}" alt="ERHA Ultimate - Klinik Spesialis Kulit &amp;amp; Rambut">
                    </a>
                </div>
            </div>

            <div class="search-input mobile ">
                <span class="close-search" data-target="mobile">
                    <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 384 512" fill="currentColor"><path d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"></path></svg>
                </span>
                <form action="https://erhaultimate.co.id/search" method="get" id="form-search-mobile" class="form-search">
                    <input type="text" name="search" value="" class="header-search-bar input-search" placeholder="Pencarian">
                </form>
            </div>

            <div class="icon-wrapper d-flex align-items-center">
                <div class="search " data-target="mobile">
                    <a href="#" class="btn-search" data-target="mobile">
                        <img src="{{ asset('/img/layout/icon-search.png') }}" alt="Erha Search">
                    </a>
                </div>
                <div class="profile">
                    <a href="https://erhaultimate.co.id/profile">
                        <img src="{{ asset('/img/layout/icon-user.png') }}" alt="User Profile">
                    </a>
                </div>
            </div>
        </nav>

        <div id="sidenav" class="sidenav">
            <div class="d-flex align-items-center justify-content-between side-logo-wrapper">
                <img class="logo-mobile" src="{{ asset('/img/layout/logo.png') }}" alt="ERHA Ultimate - Klinik Spesialis Kulit &amp;amp; Rambut">
                <span id="close-nav" class="close-btn">
                    <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 384 512" fill="currentColor"><path d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"></path></svg>
                </span>
            </div>

            <ul class="mobile-menu-wrapper links">
                <li class="">
                    <a href="#" target="_self" class="title parent-menu ">
                        Solusi Sesuai Problem  <span></span>                    
                        <span class="icon-arrow-wrapper"><img class="icon-arrow" data-src="{{ asset('/img/layout/loader.png') }}" src="{{ asset('/img/layout/loader.png') }}" alt="arrow"></span>
                    </a>

                    <ul class="htmlCss-sub-menu sub-menu links">
                        <li class="">
                            <a href="https://erhaultimate.co.id/acne-center" target="_self" class="title-subtitle">
                                Acne Center <span style="display: block">Kulit Jerawat &amp; Bekas Jerawat</span> 
                            </a>
                        </li>
                        <li class="">
                            <a href="https://erhaultimate.co.id/anti-aging-center" target="_self" class="title-subtitle">
                                Anti Aging Center <span style="display: block">Kulit Kendur &amp; Garis Halus</span> 
                            </a>
                        </li>
                        <li class="">
                            <a href="https://erhaultimate.co.id/brightening-center" target="_self" class="title-subtitle">
                                Brightening Center <span style="display: block">Kulit Kusam &amp; Noda Hitam</span> 
                            </a>
                        </li>
                        <li class="">
                            <a href="https://erhaultimate.co.id/hair-care-center" target="_self" class="title-subtitle">
                                Hair Care Center <span style="display: block">Rambut Rontok &amp; Botak</span> 
                            </a>
                        </li>
                        <li class="">
                            <a href="https://erhaultimate.co.id/make-over-center" target="_self" class="title-subtitle">
                                Make Over Center <span style="display: block">Bentuk Wajah &amp; Badan Kurang Ideal</span> 
                            </a>
                        </li>
                        <li class="">
                            <a href="https://erhaultimate.co.id/atopy-and-skin-disease-center" target="_self" class="title-subtitle">
                                Atopy &amp; Skin Disease Center <span style="display: block">Dermatitis Atopik &amp; Penyakit Kulit</span> 
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="https://erhaultimate.co.id/wiki" target="_self" class="title">
                        Skin Wiki <span></span> 
                    </a>
                </li>
                <li class="">
                    <a href="https://erhaultimate.co.id/booking" target="_self" class="title">
                        Buat Jadwal ke Klinik <span></span> 
                    </a>
                </li>
                <li class="">
                    <a href="{{ env('APP_URL').'find-clinic/' }}" target="_self" class="title">
                        Cari Klinik <span></span> 
                    </a>
                </li>
                <li class="">
                    <a href="https://erhaultimate.co.id/promo" target="_self" class="title">
                        Promo Spesial <span></span> 
                    </a>
                </li>
                <li class="">
                    <a href="https://erhaultimate.co.id/erha-story" target="_self" class="title">
                        Kisah ERHA <span></span> 
                    </a>
                </li>
                <li class="">
                    <a href="https://erhaultimate.co.id/article" target="_self" class="title">
                        Info Terkini <span></span> 
                    </a>
                </li>
                <li class="">
                    <a href="https://wa.me/6281121212121" target="_self" class="title">
                        Hubungi Kami <span></span> 
                    </a>
                </li>
            </ul>
        </div>
    </header>
    
    <div id="page-wrap">
        <div class="no-sidebar">
            @yield('content')
        </div>
    </div>

    <footer id="footer-elem" class="show-on-scroll">
        <div class="footer-wrapper">
            <div class="footer container">

                <div class="footer-left">
                    <div class="footer-container">
                        <div class="footer-left-flex">

                            <div class="flex-1 footer-items-wrapper">
                                <div class="footer-logo">
                                    <img src="{{ asset('/img/layout/erha-ultimate.png') }}" class="card-img-top" alt="Erha Ultimate New E Logotype (2)">
                                </div>
                            </div>

                            <div class="flex-2 footer-items-wrapper">
                                <aside class="footer-content">
                                    <div class="footer-title">
                                        <h4>Hubungi Kami</h4>
                                    </div>
                                    <div class="widget__content">
                                        <div class="person-detail">
                                            <p>DISTRICT 8 Building Treasury Tower <br>36th &amp; 37th Floor SCBD Lot 28 <br>Jl. Jend Sudirman Kav. 52 – 53 Jakarta 12190</p>
                                            <p><img class="logo-contacts" src="{{ asset('/img/layout/icon-7.png') }}" alt="logo mail"> <a href="mailto:dear@erha.co.id">dear@erha.co.id</a></p>
                                            <p><img class="logo-contacts" src="{{ asset('/img/layout/icon-5.png') }}" alt="logo phone"> <a href="tel:02150922121">(021) 5092&nbsp;2121</a></p>
                                            <p><img class="logo-contacts" src="{{ asset('/img/layout/wa-logo.png') }}" alt="logo wa"> <a href="https://wa.me/6281121212121" target="_blank" class="whatsapp-link">+62-811-2121-2121</a></p>
                                        </div>
                                    </div>
                                </aside>
                                <aside class="footer-content mt-5 pb-5 hide-desktop-tablet-ls">
                                    <div class="footer-title mb-5 pb-5">
                                        <h4><a href="https://erhaultimate.co.id/privacy-policy">Kebijakan Privasi</a></h4>
                                    </div>
                                </aside>
                            </div>

                            <div class="flex-3 footer-items-wrapper">
                                <aside class="footer-content">
                                    <div class="footer-title">
                                        <h4>Ikuti Kami</h4>
                                    </div>
                                    <div class="widget__content">
                                        <div class="person-detail">
                                            <ul class="social-follow">
                                                <li>
                                                    <a href="https://www.facebook.com/erhadermatology.official" title="Facebook" target="_blank">
                                                        <img class="logo-social" src="{{ asset('/img/layout/facebook.png') }}" alt="Facebook">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.instagram.com/erha.ultimate/" title="Instagram" target="_blank">
                                                        <img class="logo-social" src="{{ asset('/img/layout/instagram.png') }}" alt="Instagram">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.youtube.com/@dearerha" title="Youtube" target="_blank">
                                                        <img class="logo-social" src="{{ asset('/img/layout/youtube.png') }}" alt="Youtube">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.tiktok.com/@erha.ultimate" title="Tiktok" target="_blank">
                                                        <img class="logo-social" src="{{ asset('/img/layout/tiktok.png') }}" alt="Tiktok">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </aside>
                                <aside class="footer-content mt-5 hide-mobile-tablet-ls">
                                    <div class="footer-title">
                                        <h4><a href="https://erhaultimate.co.id/privacy-policy">Kebijakan Privasi</a></h4>
                                    </div>
                                </aside>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="footer-right">
                    <div class="footer-container">
                        <div class="footer-right-flex">
                            <div class="flex-right-1">
                                <img class="img-phone" src="{{ asset('/img/layout/footer-phone.png') }}" alt="Product">
                            </div>
                            <div class="flex-right-2">
                                <h4 class="footer-title-right">ERHA Buddy</h4>
                                <h5>Download Sekarang</h5>
                                <a href="https://play.google.com/store/apps/details?id=id.erha.userapp&amp;pcampaignid=web_share" target="_blank">
                                    <img class="link-app" src="{{ asset('/img/layout/playstore.png') }}" alt="playstore icon">
                                </a>
                                <a href="https://apps.apple.com/id/app/erha-buddy/id1507965731?l=id" target="_blank">
                                    <img class="link-app" src="{{ asset('/img/layout/appstore.png') }}" alt="appstore icon">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>

    <div id="back2top" class="btn-circle show-on-scroll">
        <a href="#">
            <img data-src="{{ asset('/img/layout/scroll-to-top.png') }}" src="{{ asset('/img/layout/scroll-to-top.png') }}" alt="scrolls">
        </a>
    </div>

    <div class="floating-buttons">
        <div class="container">
            <ul>
                <li class="">
            <a class="btn-menu " href="https://erhaultimate.co.id/solution" target="_self">
                <img data-src="{{ asset('/img/layout/icon-6.png') }}" src="{{ asset('/img/layout/icon-6.png') }}" class="logo-menu" alt="icon CARI SOLUSI"> 
                <span>CARI SOLUSI</span> 
            </a>
        </li>
            <li class="">
            <a class="btn-menu " href="https://erhaultimate.co.id/wiki" target="_self">
                <img data-src="{{ asset('/img/layout/icon-3.png') }}" src="" class="logo-menu" alt="icon SKIN WIKI"> 
                <span>SKIN WIKI</span> 
            </a>
        </li>
            <li class="">
            <a class="btn-menu " href="https://erhaultimate.co.id/booking" target="_self">
                <img data-src="{{ asset('/img/layout/icon-4.png') }}" src="{{ asset('/img/layout/icon-4.png') }}" class="logo-menu" alt="icon BUAT JADWAL"> 
                <span>BUAT JADWAL</span> 
            </a>
        </li>
            <li class="">
            <a class="btn-menu " href="{{ env('APP_URL').'find-clinic/' }}" target="_self">
                <img data-src="{{ asset('/img/layout/icon-8.png') }}" src="{{ asset('/img/layout/icon-8.png') }}" class="logo-menu" alt="icon CARI KLINIK &amp; DOKTER"> 
                <span>CARI KLINIK &amp; DOKTER</span> 
            </a>
        </li>
            <li class="">
            <a class="btn-menu whatsapp-link" href="https://wa.me/6281121212121" target="_blank">
                <img data-src="{{ asset('/img/layout/wa-logo.png') }}" src="{{ asset('/img/layout/wa-logo.png') }}" class="logo-menu" alt="icon HUBUNGI KAMI"> 
                <span>HUBUNGI KAMI</span> 
            </a>
        </li>
        
            </ul>
        </div>
    </div>

    <script src="{{ asset('/js/language-public.js') }}"></script>
    <!-- <script type="text/javascript" async="" src="https://analytics.tiktok.com/i18n/pixel/events.js?sdkid=CKTMQ2JC77U115KEEG30&amp;lib=ttq"></script> -->
    <!-- <script type="text/javascript" async="" src="{{ asset('/js/fbevents.js') }}"></script> -->
    <!-- <script type="text/javascript" async="" src="{{ asset('/js/js') }}"></script> -->
    <!-- <script async="" src="{{ asset('/js/gtm.js') }}"></script> -->
    <script src="{{ asset('/js/bootstrap.min.js') }}" defer=""></script>

    @foreach ($js as $j)
        <script src="{{ asset('/js/'.$j) }}"></script>
    @endforeach

    <script src="{{ asset('/js/slick.min.js') }}" integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A==" crossorigin="anonymous" referrerpolicy="no-referrer" defer=""></script>

    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', () => {
            function isViewportNotAtTop() {
                return window.scrollY > 0;
            }
            function handleNotAtTop() {
                const footerElems = document.querySelectorAll('.show-on-scroll');
                footerElems.forEach((footerElem) => {
                    footerElem.classList.remove('d-none');
                });
            }
            if (isViewportNotAtTop()) {
                handleNotAtTop();
            }
            window.addEventListener('scroll', () => {
                if (isViewportNotAtTop()) {
                    handleNotAtTop();
                }
            });
        });
    </script>


    <script src="{{ asset('/js/common.js') }}" defer=""></script>
    <script src="{{ asset('/js/slick-init.js') }}" defer=""></script>
    <script src="{{ asset('/js/lazy-load.js') }}" defer=""></script>
    <script src="{{ asset('/js/ga-event.js') }}" defer=""></script>

    <!-- <script type="text/javascript" id="">!function(d,g,e){d.TiktokAnalyticsObject=e;var a=d[e]=d[e]||[];a.methods="page track identify instances debug on off once ready alias group enableCookie disableCookie".split(" ");a.setAndDefer=function(b,c){b[c]=function(){b.push([c].concat(Array.prototype.slice.call(arguments,0)))}};for(d=0;d<a.methods.length;d++)a.setAndDefer(a,a.methods[d]);a.instance=function(b){b=a._i[b]||[];for(var c=0;c<a.methods.length;c++)a.setAndDefer(b,a.methods[c]);return b};a.load=function(b,c){var f="https://analytics.tiktok.com/i18n/pixel/events.js";
    a._i=a._i||{};a._i[b]=[];a._i[b]._u=f;a._t=a._t||{};a._t[b]=+new Date;a._o=a._o||{};a._o[b]=c||{};c=document.createElement("script");c.type="text/javascript";c.async=!0;c.src=f+"?sdkid\x3d"+b+"\x26lib\x3d"+e;b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(c,b)};a.load("CKTMQ2JC77U115KEEG30");a.page()}(window,document,"ttq");</script> -->

</body>
</html>