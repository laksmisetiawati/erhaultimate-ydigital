<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Application\ClinicController;
use App\Http\Controllers\Application\DoctorController;
use App\Http\Controllers\Application\HomeController;
// use App\Http\Controllers\Application\ScrapeController;

Route::get( '/', [HomeController::class, 'home'] );
Route::get( '/truncate', [HomeController::class, 'truncate'] );

Route::prefix('find-doctor')->group(function () {
    Route::get( '/', [DoctorController::class, 'find_doctor'] );
    Route::get( '{slug}', [DoctorController::class, 'find_doctor_detail'] );
});

Route::prefix('find-clinic')->group(function () {
    Route::get( '/', [ClinicController::class, 'find_clinic'] );
    Route::get( '{slug}', [ClinicController::class, 'find_clinic_detail'] );
});